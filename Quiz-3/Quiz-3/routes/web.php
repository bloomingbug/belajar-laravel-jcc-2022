<?php

Route::get('/', function () {
    return view('welcome');
});

// CRUD Game
// Create
Route::get('/game/create', 'GameController@create');
Route::post('/game', 'GameController@store');

// Read
Route::get('/game', 'GameController@index');
Route::get('/game/{game_id}', 'GameController@show');

// Update
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('/game/{game_id}', 'GameController@update');

// Delete
Route::delete('/game/{game_id}', 'GameController@destroy');