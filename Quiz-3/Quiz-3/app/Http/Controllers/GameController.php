<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{   
    // CREATE
    // Menampilkan form create
    public function create() {
        return view('game.create');
    }

    // Mengirim submit data ke DB
    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ],
        [
        'name.required' => 'Kolom nama mohon diisi!',
        'gameplay.required'  => 'Kolom gameplay mohon diisi!',
        'developer.required'  => 'Kolom developer mohon diisi!',
        'year.required'  => 'Kolom year mohon diisi!',
        ]);

        DB::table('game')->insert(
        [
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year']
        ]);

        return redirect('/game');
    }

    // READ
    // Menampilkan data table game
    public function index(){
        $game = DB::table('game')->get();
        return view('game.index', compact('game'));
    } 

    // Menampilkan detail
    public function show($game_id) {
        $game = DB::table('game')->where('id', $game_id)->first();
        return view('game.show', compact('game'));
    }

    // UPDATE
    // Memilih item yang akan diedit
    public function edit($game_id) {
        $game = DB::table('game')->where('id', $game_id)->first();
        return view('game.edit', compact('game'));
    }

    // Mengirim data edit ke DB
    public function update($game_id, Request $request) {
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ],
        [
        'name.required' => 'Kolom nama mohon diisi!',
        'gameplay.required'  => 'Kolom gameplay mohon diisi!',
        'developer.required'  => 'Kolom developer mohon diisi!',
        'year.required'  => 'Kolom year mohon diisi!',
        ]);
        
        DB::table('game')
            ->where('id', $game_id)
            ->update([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year']
        ]);
            
        return redirect('/game');
    }

    // DELETE
    // Menghapus data
    public function destroy($game_id) {
        DB::table('game')
        ->where('id', $game_id)
        ->delete();

        return redirect('/game');
    }
}