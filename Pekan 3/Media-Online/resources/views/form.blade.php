<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form - Tugas Pertemuan 1</title>
</head>

<body>
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>

    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First name:</label>
        <br /><br />
        <input type="text" id="fname" name="fname" required />
        <br /><br />

        <label for="lname">Last name:</label>
        <br /><br />
        <input type="text" id="lname" name="lname" required />
        <br /><br />

        <label>Gender</label>
        <br /><br />
        <input type="radio" name="gender" value="1" required />Male
        <br />
        <input type="radio" name="gender" value="2" required />Female
        <br /><br />

        <label>Nationality</label>
        <select name="nationality" required>
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select>
        <br /><br />

        <label>Language Spoken</label>
        <br /><br />
        <input type="checkbox" name="language" />Bahasa Indonesia <br />
        <input type="checkbox" name="language" />English <br />
        <input type="checkbox" name="language" />Other <br /><br />

        <label for="bio">Bio</label>
        <br /><br />
        <textarea id="bio" name="bio" cols="30" rows="10"></textarea>
        <br />
        <input type="submit" value="Sign Up" />
</body>

</html>