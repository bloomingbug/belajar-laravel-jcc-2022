<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller {
    public function register (){
        return view('pages.register');
    }

    public function welcome(Request $request){
        // dd($request->all());
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('pages.welcome', compact('fname', 'lname'));
    }
}