<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Film;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();

        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $genre = Genre::all();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpeg,jpg,png|max:10000',
            'genre_id' => 'required',
        ],
        [
        'judul.required' => 'Kolom judul harus diisi yah kawan!',
        'ringkasan.required' => 'Kolom ringkasan harus diisi yah kawan!',
        'tahun.required' => 'Kolom tahun harus diisi yah kawan!',
        'poster.required' => 'Kolom poster harus diisi yah kawan!',
        'poster.mimes' => 'Format poster harus .jpg, .jpeg, .png',
        'poster.max' => 'Ukuran poster maksimal 10MB',
        'genre_id.required' => 'Kolom genre harus diisi yah kawan!',
        ]);

        $gambar = $request->poster;
        $new_gambar = 'RP' . time() . ' - ' . $gambar->getClientOriginalName();

        $film = new Film;

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $new_gambar;
        $film->genre_id = $request->genre_id;

        $gambar->move('poster/', $new_gambar);

        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);

        return view('film.show', compact('film')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        
        return view('film.edit', compact('film', 'genre'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpeg,jpg,png|max:10000',
            'genre_id' => 'required',
        ],
        [
        'judul.required' => 'Kolom judul harus diisi yah kawan!',
        'ringkasan.required' => 'Kolom ringkasan harus diisi yah kawan!',
        'tahun.required' => 'Kolom tahun harus diisi yah kawan!',
        'poster.required' => 'Kolom poster harus diisi yah kawan!',
        'poster.mimes' => 'Format poster harus .jpg, .jpeg, .png',
        'poster.max' => 'Ukuran poster maksimal 10MB',
        'genre_id.required' => 'Kolom genre harus diisi yah kawan!',
        ]);

        $gambar = $request->poster;
        $new_gambar = 'RP' . time() . ' - ' . $gambar->getClientOriginalName();

        $film = Film::find($id);

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $new_gambar;
        $film->genre_id = $request->genre_id;

        $gambar->move('poster/', $new_gambar);

        $film->update();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        $film->delete();

        return redirect('/film');
    }
}