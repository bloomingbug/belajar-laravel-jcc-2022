@extends('layouts.master')

@section('judul')
    Detail of {{$film->nama}}
@endsection

@section('content')

<a href="/film" class="btn btn-secondary btn-md">Kembali</a>

<table class="table table-borderless table-sm mt-3">
    <tbody>
        <tr>
            <td>ID</td>
            <td>{{$film->id}}</td>
        </tr>
        <tr>
            <td>Judul</td>
            <td>{{$film->judul}}</td>
        </tr>
        <tr>
            <td>Ringkasan</td>
            <td>{{$film->ringkasan}}</td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td>{{$film->tahun}}</td>
        </tr>
        <tr>
            <td>Genre</td>
            <td>{{$film->genre_id}}</td>
        </tr>
        <tr>
            <td>Genre</td>
            <td><img src="{{asset('poster/' . $film->poster)}}" alt=""></td>
        </tr>
    </tbody>
</table>
@endsection