@extends('layouts.master')

@section('judul')
    List Film
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary btn-md">Tambah Film</a>
    <div class="row">
        @forelse ($film as $key => $item)
            <div class="card m-2" style="width: 18rem;">
                <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top overflow-hidden rounded-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{$item->judul}}</h5>
                    <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
                    <div class="d-flex flex-wrap justify-content-around mx-auto">
                            <a href="/film/{{$item->id}}" class="btn btn-info btn-sm m-1">Detail</a>
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm m-1">Update</a>
                            <form action="/film/{{$item->id}}" method="post" class="m-1 p-0">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm mx-auto w-100">Delete</button>
                            </form>
                    </div>
                </div>
            </div>
            @empty
            <h2>Data Masih Kosong!</h2>
            @endforelse
    </div>
@endsection