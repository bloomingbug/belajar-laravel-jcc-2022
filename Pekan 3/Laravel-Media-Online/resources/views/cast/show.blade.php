@extends('layouts.master')

@section('judul')
    Detail of {{$cast->nama}}
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-md">Tambah Cast</a>

<table class="table table-borderless table-sm mt-3">
    <tbody>
        <tr>
            <td>ID</td>
            <td>{{$cast->id}}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{{$cast->nama}}</td>
        </tr>
        <tr>
            <td>Umur</td>
            <td>{{$cast->umur}}</td>    
        </tr>
        <tr>
            <td>Bio</td>
            <td>{{$cast->bio}}</td>
        </tr>
    </tbody>
</table>
@endsection