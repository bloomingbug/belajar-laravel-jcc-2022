@extends('layouts.master')

@section('judul')
    List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-md">Tambah Cast</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <div class="d-flex flex-wrap flex-column flex-md-row justify-center">
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm m-1">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm m-1">Update</a>
                        <form action="/cast/{{$item->id}}" method="post" class="m-1 p-0">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm mx-auto w-100">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
            
        @empty
            
        @endforelse
    </tbody>
</table>
@endsection