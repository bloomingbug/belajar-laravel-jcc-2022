@extends('layouts.master')

@section('judul')
    Pendaftaran Berhasil!
@endsection

@section('content')
    <h1>Selamat Datang {{$fname}} {{$lname}}</h1>
    <h3>
        Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!
    </h3>
@endsection