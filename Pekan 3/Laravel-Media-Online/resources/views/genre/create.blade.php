@extends('layouts.master')

@section('judul')
    Tambahkan Cast
@endsection

@section('content')
<form action="/genre" method="post">
    @csrf
    <div class="form-group">
        <label for="nama">Nama Genre</label>
        <input type="text" class="form-control" id="nama" name="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection