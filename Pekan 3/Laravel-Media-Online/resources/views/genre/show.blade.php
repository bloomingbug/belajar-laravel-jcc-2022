@extends('layouts.master')

@section('judul')
    Detail of {{$genre->nama}}
@endsection

@section('content')

<a href="/genre" class="btn btn-secondary btn-md">Kembali</a>

<table class="table table-borderless table-sm mt-3">
    <tbody>
        <tr>
            <td>ID</td>
            <td>{{$genre->id}}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>{{$genre->nama}}</td>
        </tr>
    </tbody>
</table>
@endsection