@extends('layouts.master')

@section('judul')
    List Cast
@endsection

@section('content')

<a href="/genre/create" class="btn btn-primary btn-md">Tambah Cast</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <div class="d-flex flex-wrap flex-column flex-md-row justify-center">
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm m-1">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm m-1">Update</a>
                        <form action="/genre/{{$item->id}}" method="post" class="m-1 p-0">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm mx-auto w-100">Delete</button>
                        </form>
                    </div>
                </td>
            </tr>
            
        @empty
            <h2>Data Masih Kosong!</h2>
        @endforelse
    </tbody>
</table>
@endsection