<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::any('/table', 'TableController@table');
Route::any('/data-tables', 'TableController@dataTable');
    
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

// CRUD Cast
// Create
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

// Read
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');

// CRUD Genre with ORM
Route::resource('genre', 'GenreController');

// CRUD Film
Route::resource('film', 'FilmController');